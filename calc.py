from kivy.app import App

from kivy.core.window import Window
Window.size = (600,800)


class CalcApp(App):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.title = 'KivyCalclator'


if __name__ == '__main__':
    CalcApp().run()

